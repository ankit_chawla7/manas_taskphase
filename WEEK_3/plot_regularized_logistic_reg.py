#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 28 23:13:46 2019

@author: blackfly
"""

import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv("ex2data2.txt",header = None)
X = data.iloc[:,0:2]
y = data.iloc[:,2]

mask = y == 1

acc = plt.scatter(X[mask][0],X[mask][1],c = 'black',marker = '+')
rej = plt.scatter(X[~mask][0],X[~mask][1],c = 'yellow')
plt.xlabel("Microchip Test1")
plt.ylabel("Microchip Text2")
plt.legend((acc,rej),("Passed","Failed"))
plt.show()