#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 28 23:32:15 2019

@author: blackfly
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt

def sigmoid(x):
    return 1/(1+np.exp(-x))

def costfunc(theta,X,y,lamda):
    h = np.dot(X,theta)
    h = sigmoid(h)
    m = len(y)
    reg = (lamda/(2*m))*np.sum(np.power(theta[1:],2))
    cost = np.sum(np.dot(y.T,np.log(h))+np.dot((1-y).T,np.log(1-h))) 
    return -cost/m + reg

def gradient(theta,X,y,lamda):
    h = np.dot(X,theta)
    h = sigmoid(h)
    m = len(y)
    grad = np.dot(X.T,(h - y))/m;
    grad[1:] = grad[1:] + (lamda/m)*theta[1:]
    return grad

def mapFeature(X1, X2):
    degree = 6
    out = np.ones(X.shape[0])
    out = out[:,np.newaxis]
    for i in range(1, degree+1):
        for j in range(i+1):
                        out = np.hstack((out, np.multiply(np.power(X1, i-j),np.power(X2, j))[:,np.newaxis]))
    return out

def accuracy(X,y,theta,cutoff):
    pred = [sigmoid(np.dot(X, theta)) >= 0.5]
    acc = np.mean(pred == y.flatten()) * 100
    print(acc,"%")
    
"""def mapFeatureForPlotting(X1, X2):
    degree = 6
    out = np.ones(1)
    for i in range(1, degree+1):
        for j in range(i+1):
            out = np.hstack((out, np.multiply(np.power(X1, i-j), np.power(X2, j))))
    return out"""


data = pd.read_csv("ex2data2.txt",header = None)
X = data.iloc[:,0:2]
y = data.iloc[:,2]
y = y[:,np.newaxis]
X = mapFeature(X.iloc[:,0], X.iloc[:,1])
(m,n) = X.shape
lamda = 1
theta = np.zeros((n,1))
cost = costfunc(theta,X,y,lamda)
print(cost)

output = opt.fmin_tnc(func = costfunc,x0 = theta.flatten(),fprime = gradient,args = (X,y.flatten(),lamda))
theta = output[0]
cost = costfunc(theta,X,y,lamda)
print(cost)
accuracy(X,y.flatten(),theta,0.5)

"""u = np.linspace(-1, 1.5, 50)
v = np.linspace(-1, 1.5, 50)
z = np.zeros((len(u), len(v)))
for i in range(len(u)):
    for j in range(len(v)):
        z[i,j] = np.dot(mapFeatureForPlotting(u[i], v[j]), theta)
mask = y.flatten() == 1
X = data.iloc[:,:-1]
passed = plt.scatter(X[mask][0], X[mask][1])
failed = plt.scatter(X[~mask][0], X[~mask][1])
plt.contour(u,v,z,0)
plt.xlabel('Microchip Test1')
plt.ylabel('Microchip Test2')
plt.legend((passed, failed), ('Passed', 'Failed'))
plt.show()"""




