#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 01:53:00 2019

@author: blackfly
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize as opt
def sigmoid(x):
    return 1/(1+np.exp(-x))

def costfunc(theta,X,y):
    h = np.dot(X,theta)
    h = sigmoid(h)
    m = len(y)
    cost = np.sum(np.dot(y.T,np.log(h))+np.dot((1-y).T,np.log(1-h)))
    return -cost/m


def gradient(theta,X,y):
    return (1/m)*np.dot(X.T,(sigmoid(np.dot(X,theta)) - y))

def accuracy(X,y,theta,cutoff):
    pred = [sigmoid(np.dot(X, theta)) >= 0.5]
    acc = np.mean(pred == y.flatten()) * 100
    print(acc,"%")



data = pd.read_csv("ex2data1.txt",header = None)
X = data.iloc[:,0:2]
y = data.iloc[:,2]
y = y[:,np.newaxis]
(m,n) = X.shape
theta = np.zeros((n+1,1))
ones = np.ones((m,1))

X = np.hstack((ones,X))
cost = costfunc(theta,X,y)
print(cost) #before optimizing
temp = opt.fmin_tnc(func = costfunc,x0 = theta.flatten(),fprime = gradient,args = (X, y.flatten()))
theta = temp[0]
cost = costfunc(theta,X,y)
print(cost) #after optimizing

#plotting graph
plot_x = [np.min(X[:,1]),np.max(X[:,1])]
plot_y = -((np.dot(theta[1],plot_x) + theta[0])/theta[2]);
mask = y.flatten() == 1
adm = plt.scatter(X[mask][:,1], X[mask][:,2],c = 'black',marker = '+')
notadm = plt.scatter(X[~mask][:,1], X[~mask][:,2],c = 'yellow')
plt.plot(plot_x,plot_y)
plt.xlabel("Exam 1 Score")
plt.ylabel("Exam 2 Score")
plt.legend((adm,notadm),('Admitted','Not Admitted'))
plt.show()

accuracy(X,y.flatten(),theta,0.5)
