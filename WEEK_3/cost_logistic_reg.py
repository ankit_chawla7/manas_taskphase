#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 19:22:27 2019

@author: blackfly
"""
import numpy as np
import pandas as pd
def sigmoid(x):
    return 1/(1+np.exp(-x))

def costfunc(X,y,theta,m):
    h = np.dot(X,theta)
    h = sigmoid(h)
    cost = np.sum(np.dot(y.T,np.log(h))+np.dot((1-y).T,np.log(1-h)))
    return -cost/m

def gradientDescent(X,y,theta,alpha,itr):
    for i in range(itr):
        h = np.dot(X,theta)
        h = sigmoid(h)
        diff = h - y
        derivative = np.dot(X.T,diff)
        theta = theta - (alpha/m)*derivative
    return theta

data = pd.read_csv("ex2data1.txt",header = None)
X = data.iloc[:,0:2]
y = data.iloc[:,2]
y = y[:,np.newaxis]
(m,n) = X.shape
theta = np.zeros((n+1,1))
ones = np.ones((m,1))
X = np.hstack((ones,X))
cost = costfunc(X,y,theta,m)
print(cost)
theta = gradientDescent(X,y,theta,0.001,1500)
cost = costfunc(X,y,theta,m)
print(cost)

