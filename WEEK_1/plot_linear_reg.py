#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 22:52:49 2019

@author: blackfly
"""
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv("ex1data1.txt")
X = data.iloc[:,0]
y = data.iloc[:,1]
m = len(y)
plt.scatter(X,y,c = 'red',marker = 'x')
plt.xlabel("Population of city in 10,000's")
plt.ylabel("Profit in $10,000s")
plt.show() #using only when implementing through terminal

