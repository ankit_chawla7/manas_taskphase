#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 23:30:11 2019

@author: blackfly
"""

import pandas as pd
import numpy as np

def computeCost(X,y,theta,m):
    h = np.dot(X,theta)
    cost = np.sum(np.square(h-y))/(2*m)
    return cost

data = pd.read_csv("ex1data1.txt",header = None)
X = data.iloc[:,0]
y = data.iloc[:,1]
m = len(y)
X = X[:,np.newaxis]
y = y[:,np.newaxis]
theta = np.zeros([2,1])
ones = np.ones([m,1])
X = np.hstack((ones,X))
J = computeCost(X,y,theta,m)
print(J)

