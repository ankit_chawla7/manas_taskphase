#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 00:19:00 2019

@author: blackfly
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def computeCost(X,y,theta,m):
    h = np.dot(X,theta)
    cost = np.sum(np.square(h-y))/(2*m)
    return cost

def gradientDescent(X,y,theta,alpha,itr):
    for i in range(itr):
        h = np.dot(X,theta)
        diff = h - y
        derivative = np.dot(X.T,diff)
        theta = theta - (alpha/m)*derivative
    return theta
        
data = pd.read_csv("ex1data1.txt",header = None)
X = data.iloc[:,0]
y = data.iloc[:,1]
m = len(y)
X = X[:,np.newaxis]
y = y[:,np.newaxis]
theta = np.zeros([2,1])
itr = 1500
alpha = 0.01
ones = np.ones([m,1])
X = np.hstack((ones,X))
J = computeCost(X,y,theta,m)
print(J)
theta = gradientDescent(X,y,theta,alpha,itr)
print(theta)
J = computeCost(X,y,theta,m)
print(J)
plt.scatter(X[:,1],y,c = 'red',marker = 'x')
plt.plot(X[:,1],np.dot(X,theta))            #to understand
#plt.show()
