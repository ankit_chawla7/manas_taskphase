#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 20:44:58 2019

@author: blackfly
"""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize as opt
def sigmoid(x):
    return 1/(1+np.exp(-x))

def costfunc(theta,X,y):
    h = np.dot(X,theta)
    h = sigmoid(h)
    m = len(y)
    cost = np.sum(np.dot(y.T,np.log(h))+np.dot((1-y).T,np.log(1-h)))
    return -cost/m


def gradient(theta,X,y):
    return (1/m)*np.dot(X.T,(sigmoid(np.dot(X,theta)) - y))

def accuracy(X,y,theta,cutoff):
    pred = [sigmoid(np.dot(X, theta)) >= 0.5]
    acc = np.mean(pred == y.flatten()) * 100
    print(acc,"%")



train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")
answers = pd.read_csv("gender_submission.csv")
train['Age'] = train.Age.fillna(train.Age.median())
train['Fare'] = train.Fare.fillna(train.Fare.median())

test['Age'] = test.Age.fillna(test.Age.median())
test['Fare'] = test.Fare.fillna(test.Fare.median())

X_train = train[['Age','Fare']]
y_train = train['Survived']
X_test = test[['Age','Fare']]
y_test = answers['Survived']

y_train = y_train[:,np.newaxis]
y_test = y_test[:,np.newaxis]

(m,n) = X_train.shape
(m1,n1) = X_test.shape
theta = np.zeros((n+1,1))
ones = np.ones((m,1))
ones_test = np.ones((m1,1))

X_train = np.hstack((ones,X_train))
X_test = np.hstack((ones_test,X_test))

#cost = costfunc(theta,X,y)
#print(cost) #before optimizing
temp = opt.fmin_tnc(func = costfunc,x0 = theta.flatten(),fprime = gradient,args = (X_train, y_train.flatten()))
theta = temp[0]
#cost = costfunc(theta,X,y)
#print(cost) #after optimizing
accuracy(X_train,y_train.flatten(),theta,0.5)
accuracy(X_test,y_test.flatten(),theta,0.5)
#plotting graph
"""plot_x = [np.min(X[:,1]),np.max(X[:,1])]
plot_y = -((np.dot(theta[1],plot_x) + theta[0])/theta[2]);
mask = y.flatten() == 1
adm = plt.scatter(X[mask][:,1], X[mask][:,2],c = 'black',marker = '+')
notadm = plt.scatter(X[~mask][:,1], X[~mask][:,2],c = 'yellow')
plt.plot(plot_x,plot_y)
plt.xlabel("Exam 1 Score")
plt.ylabel("Exam 2 Score")
plt.legend((adm,notadm),('Admitted','Not Admitted'))
plt.show()"""





