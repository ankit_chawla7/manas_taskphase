#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 00:50:29 2019

@author: blackfly
"""

import pandas as pd
import numpy as np
def computeCost(X,y,theta,m):
    h = np.dot(X,theta)
    cost = np.sum(np.square(h-y))/(2*m)
    return cost


data = pd.read_csv("ex1data2.txt",header = None)
X = data.iloc[:,0:2]
y = data.iloc[:,2]
m = len(y)
y = y[:,np.newaxis]
theta = np.zeros([3,1])
ones = np.ones((m,1))
X = np.hstack((ones,X))
J = computeCost(X,y,theta,m)
print(J)

