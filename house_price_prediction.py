#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 29 02:19:54 2019

@author: blackfly
"""

import pandas as pd
import numpy as np
def computeCost(X,y,theta,m):
    h = np.dot(X,theta)
    cost = np.sum(np.square(h-y))/(2*m)
    return cost

def gradientDescent(X,y,theta,alpha,itr):
    for i in range(itr):
        h = np.dot(X,theta)
        diff = h - y
        derivative = np.dot(X.T,diff)
        theta = theta - (alpha/m)*derivative
    return theta

data = pd.read_fwf("housing.csv",header = None)
X = data.iloc[:,0:13]
y = data.iloc[:,13]
m = len(y)
X = (X - np.mean(X))/np.std(X)
alpha = 0.01
itr = 400
y = y[:,np.newaxis]
theta = np.zeros([14,1])
ones = np.ones((m,1))
X = np.hstack((ones,X))
J = computeCost(X,y,theta,m)
print(J)
theta = gradientDescent(X,y,theta,alpha,itr)
print(theta)
J = computeCost(X,y,theta,m)
print(J)